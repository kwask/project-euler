file = open("data.txt")

def sum_row(x, y, l, array):
    if len(array[y])-l < x:
        return 0
    else:
        product = 1
        for i in range(0, l):
            product *= array[x+i][y]
        return product

def sum_column(x, y, l, array):
    if len(array)-l < y:
        return 0
    else:
        product = 1
        for i in range(0, l):
            product *= array[x][y+i]
        return product

def sum_diagonal_r(x, y, l, array):
    if len(array[y])-l < x or len(array)-l < y:
        return 0
    else:
        product = 1
        for i in range(0, l):
            product *= array[x+i][y+i]
        return product

def sum_diagonal_l(x, y, l, array):
    if l > x or len(array)-l < y:
        return 0
    else:
        product = 1
        for i in range(0, l):
            product *= array[x-i][y+i]
        return product

array = []

for line in file:
    row = []
    for word in line.split():
        row.append(int(word))
    array.append(row)

sum = 0
cols = len(array[0])
rows = len(array)
for y in range(0, cols):
    for x in range(0, rows):
        sums = [sum_row(x, y, 4, array),
                sum_column(x, y, 4, array),
                sum_diagonal_l(x, y, 4, array),
                sum_diagonal_r(x, y, 4, array)]
        for s in sums:
            if s > sum:
                sum = s

print(sum)
