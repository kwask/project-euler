#include <cstdint>
#include <stdio.h>

#define MAX_VALUE 1000000

int main() {
    uint64_t max_len = 0;
    for(uint64_t val=1; val < MAX_VALUE; val++) {
        uint64_t len = 1;
        while(val > 1) {
            if(val%2 == 0) val = val/2; 
            else           val = val*3 + 1;

            len++;
        }
        if(len > max_len) max_len = len;
    }

    printf("the max chain had length %lu\n", max_len);
}
