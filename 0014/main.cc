#include <cstdint>
#include <stdio.h>

#define MAX_VALUE (uint64_t)1000000
#define MEMO_SIZE 3*MAX_VALUE+1

struct Node {
    uint64_t value;
    Node* next;

    Node():
        value(0), next(0)
    {}
    
    Node(uint64_t value):
        value(value), next(0)
    {}

    Node(uint64_t value, Node* next):
        value(value), next(next)
    {}
};

int main() {
    Node** memo = new Node*[MEMO_SIZE];
    for(int i=0; i < MEMO_SIZE; i++) memo[i] = 0;

    uint64_t max_val = 0;
    uint64_t max_len = 0;
    for(uint64_t val=1; val <= MAX_VALUE; val++) {
        if(!memo[val]) memo[val] = new Node(val);
        Node* cur = memo[val];

        uint64_t len = 1;
        while(cur->value > 1) {
            if(!cur->next) {
                uint64_t next_val;
                if(cur->value%2 == 0) next_val = cur->value/2; 
                else                  next_val = cur->value*3 + 1;

                if(next_val < MEMO_SIZE) {
                    if(!memo[next_val]) memo[next_val] = new Node(next_val);
                    cur->next = memo[next_val];
                } else {
                    cur->next = new Node(next_val);
                }
            }
            cur = cur->next;
            len++;
        }
        if(len > max_len) {
            max_len = len;
            max_val = val;
        }
    }

    //for(int i=0; i<MEMO_SIZE; i++) if(memo[i]) delete memo[i];

    printf("%lu had max length %lu\n", max_val, max_len);
}
