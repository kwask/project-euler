- every path is double the length of the grid
- every path has an equal number of L and R moves

given grid size 20, all paths will be length 40
40 choices from a pool of 20 L and 20 R
40! / (20! * 20!) = 137846528820
