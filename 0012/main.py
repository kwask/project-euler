import math
from collections import OrderedDict

# Will generate all the primes up to a given number using the Seive of 
# Eratosthenes
class PrimeGenerator:
    primes = []
    min = 2

    def update_primes(self, numbers):
        for (num, non_prime) in numbers.items():
            if not non_prime:
                self.primes.append(num)

    def gen_number_list(self, max):
        numbers = OrderedDict()

        if len(self.primes):
            min_after_prime = self.primes[len(self.primes)-1]+1
            if self.min < min_after_prime:
                self.min = min_after_prime

        if self.min < max:
            for num in range(self.min, max+1):
                numbers[num] = False

            for prime in self.primes:
                for num in numbers.keys():
                    if not num%prime:
                        numbers[num] = True

        return numbers

    def get_primes(self, max):
        primes_len = len(self.primes)
        if primes_len and max < self.primes[primes_len-1]:
            return self.primes

        numbers = self.gen_number_list(max)
        self.min = max
        keys = list(numbers.keys())
        keys_size = len(keys)

        # hacky thing to avoid iterating over numbers lower than the factor
        for i in range(0, keys_size):
            factor = keys[i]
            non_prime = numbers[factor]
            if not non_prime:
                for j in range(i+1, keys_size):
                    num = keys[j]
                    if not num%factor:
                        numbers[num] = True

        self.update_primes(numbers)
        return self.primes

# Find the prime factors of the given number
class PrimeFactors:
    pg = PrimeGenerator()

    def __init__(self, num):
        super().__init__()
        self.factors = []
        self.prime_factorization(num, self.pg.primes)

    def prime_factorization(self, num, primes):
        if num <= 1:
            return
        elif num in primes:
            self.factors.append(num)
        else:
            factored = False

            for prime in primes:
                if not num%prime:
                    self.factors.append(prime)
                    self.prime_factorization(int(num/prime), primes)
                    factored = True
                    break

            if not factored:
                self.prime_factorization(num, self.pg.get_primes(num))

    # Find the total number of factors for N by using the formula
    # N = (p^a)(q^b)(r^c) where p,q,r is the prime factorization
    # number of factors = (a+1)(b+1)(c+1)
    def get_num_factors(self):
        prime_totals = {}

        for prime in self.factors:
            if prime in prime_totals.keys():
                prime_totals[prime] += 1
            else:
                prime_totals[prime] = 1

        total_factors = 1
        for total in prime_totals.values():
            total_factors *= total+1

        return total_factors

PrimeFactors(100)

max_factors = 500
most_factors = 0
value = 0

tri_num = 0
i = 0

found = False

while not found:
    i += 1
    tri_num += i

    prime_factors = PrimeFactors(tri_num)
    tri_num_factors = prime_factors.get_num_factors()

    if tri_num_factors > most_factors:
        most_factors = tri_num_factors
        value = tri_num

    if most_factors >= max_factors:
        found = True

print("ANSWER: " + str(value))
print("Number of Factors: " + str(most_factors))
