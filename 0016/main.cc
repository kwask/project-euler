#include <stdio.h>
#include <vector>
#include <string>

#define N_DOUBLINGS 1000

typedef unsigned char uchar;

void print_vec(std::vector<uchar> &digits) {
    std::string out = "";
    for(size_t i = 0; i < digits.size(); i++) {
        out += std::to_string(digits.at(digits.size()-i-1));
    }
    printf("%s\n", out.c_str());
}

void double_num(std::vector<uchar> &digits) {
    size_t d_size = digits.size();
    uchar carry = 0;
    for(size_t i = 0; i < d_size; i++) {
        uchar v = digits.at(i);

        v *= 2;
        v += carry;
        carry = 0;

        while(v >= 10) {
            carry += 1;
            v -= 10; 
        }
        digits.at(i) = v;
    }
    if(carry) digits.push_back(carry);
}

int main() {
    std::vector<uchar> digits;

    // find 1000th doubling
    digits.push_back(2);
    for(size_t i = 1; i < N_DOUBLINGS; i++) {
        double_num(digits); 
    }
    //print_vec(digits);

    // sum the digits
    unsigned long long sum = 0;
    for(size_t i = 0; i < digits.size(); i++) {
        sum += digits.at(i);
    }
    printf("%s\n", std::to_string(sum).c_str());
}
