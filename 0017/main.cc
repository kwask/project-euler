#include <string>
#include <vector>
#include <stdio.h>

#define TENS_NAMES_END 100
#define TEENS_NAMES_END 20
#define ONES_NAMES_END 10

struct Number {
    int value;
    std::string name;

    Number(int value, std::string name):
        value(value), name(name)
    {}
};

std::string map_num_name(int &n, const int MAX, std::vector<Number*> names) {
    for(Number* num : names) {
        if(n >=  num->value) {
            n -= num->value;
            return num->name;
        }
    }

    return "";
}

std::string get_num_name(int n) {
    std::vector<Number*> num_names;
    num_names.push_back(new Number(1000, "thousand"));
    num_names.push_back(new Number(100, "hundred"));

    std::vector<Number*> tens_names;
    tens_names.push_back(new Number(90, "ninety"));
    tens_names.push_back(new Number(80, "eighty"));
    tens_names.push_back(new Number(70, "seventy"));
    tens_names.push_back(new Number(60, "sixty"));
    tens_names.push_back(new Number(50, "fifty"));
    tens_names.push_back(new Number(40, "forty"));
    tens_names.push_back(new Number(30, "thirty"));
    tens_names.push_back(new Number(20, "twenty"));

    std::vector<Number*> teens_names;
    teens_names.push_back(new Number(19, "nineteen"));
    teens_names.push_back(new Number(18, "eighteen"));
    teens_names.push_back(new Number(17, "seventeen"));
    teens_names.push_back(new Number(16, "sixteen"));
    teens_names.push_back(new Number(15, "fifteen"));
    teens_names.push_back(new Number(14, "fourteen"));
    teens_names.push_back(new Number(13, "thirteen"));
    teens_names.push_back(new Number(12, "twelve"));
    teens_names.push_back(new Number(11, "eleven"));
    teens_names.push_back(new Number(10, "ten"));

    std::vector<Number*> ones_names;
    ones_names.push_back(new Number(9, "nine"));
    ones_names.push_back(new Number(8, "eight"));
    ones_names.push_back(new Number(7, "seven"));
    ones_names.push_back(new Number(6, "six"));
    ones_names.push_back(new Number(5, "five"));
    ones_names.push_back(new Number(4, "four"));
    ones_names.push_back(new Number(3, "three"));
    ones_names.push_back(new Number(2, "two"));
    ones_names.push_back(new Number(1, "one"));

    std::string name = "";
    while(n >= TENS_NAMES_END) {
        for(Number* num : num_names) {
            int count = 0;
            while(n >= num->value) {
                count++;
                n -= num->value;
            }
            if(count > 0) {
                std::string count_name = map_num_name(count, ONES_NAMES_END, ones_names);
                name += count_name + " " + num->name;
            }
        }
    }

    if(name.length() > 0) name += " ";
    bool tens_named = true;
    if(n >= TEENS_NAMES_END) {
        if(name.length() > 0) name += "and ";
        name += map_num_name(n, TENS_NAMES_END, tens_names); 
    } else if (n >= ONES_NAMES_END) {
        if(name.length() > 0) name += "and ";
        name += map_num_name(n, TEENS_NAMES_END, teens_names);
    } else {
        tens_named = false;
    }

    if(n > 0 && n < ONES_NAMES_END) {
        if(name.length() > 0) {
            if(!tens_named) name += "and ";
            else            name += "-";
        }
        name += map_num_name(n, ONES_NAMES_END, ones_names);
    }

    return name;
}

int count_letters(std::string str) {
    int count = 0;
    for(char c : str) {
        if(c != ' ' && c != '-' && c != 0) count++;
    }
    return count;
}

int main() {
    int sum = 0;
    for(int i=1; i <= 1000; i++) {
        std::string name = get_num_name(i);
        sum += count_letters(name); 
        printf("%d: %s\n", i, name.c_str());
    }
    printf("count: %d\n", sum);
}
