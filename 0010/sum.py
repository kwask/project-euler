num_range = 2000000
numbers = [True]*num_range

for step in range(2, num_range):
    if numbers[step] == False:
        continue

    for i in range(step**2, num_range, step):
        numbers[i] = False

sum = 0
for i in range(2, num_range):
    if numbers[i]:
        sum += i

print(sum)
